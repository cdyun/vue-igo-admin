# Vue3项目开发骨架结构

###### 基于 vue3 + typeScript + vite5 + vue-router + pinia + element plus + pnpm。

###### element plus组件按需导入，vue功能模块自动导入

### 1. pnpm 替代 npm 管理依赖包

### 2. 引入eslint + stylelint + prettier 规范代码风格和质量

### 3. 引入husky + lint-staged + Commitlint 规范 git 工作流程

### 4. 安装一些插件

```
unplugin-vue-components 【组件自动按需导入】

unplugin-auto-import/vite 【自动导入 Composition API】

rollup-plugin-visualizer 【打包size分析工具】

vite-plugin-compression 【打包压缩工具 gzip / br 】
...
```

### 5. 命令

```
"pnpm dev",

"pnpm build",

"pnpm lint",

"pnpm lint:script",

"pnpm lint:stylelint",

"pnpm lint:prettierrc",

"pnpm preview"
```
