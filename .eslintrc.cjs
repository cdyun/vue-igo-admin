module.exports = {
	env: {
		browser: true,
		es2021: true,
		node: true
	},
	extends: [
		'eslint:recommended',
		'plugin:@typescript-eslint/recommended',
		'plugin:vue/vue3-essential',
		'./configs/.eslintrc-auto-import.json'
	],
	overrides: [
		{
			env: {
				node: true
			},
			files: ['.eslintrc.{js,cjs}'],
			parserOptions: {
				sourceType: 'script'
			}
		}
	],
	parser: 'vue-eslint-parser',
	parserOptions: {
		ecmaVersion: 'latest',
		parser: '@typescript-eslint/parser',
		sourceType: 'module'
	},
	settings: {
		'import/resolver': {
			alias: {
				map: { '@': './src' }
			}
		}
	},
	plugins: ['@typescript-eslint', 'vue'],
	rules: {
		'@typescript-eslint/no-var-requires': 0, //解决报错：Require statement not part of import statement.
		'vue/multi-word-component-names': 'off', //关闭组件命名规则验证
		'@typescript-eslint/no-explicit-any': ['off']
	},
	root: true
}
