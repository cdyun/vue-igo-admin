import { createApp } from 'vue'
import App from './App.vue'
import '@/assets/css/index.scss'
import 'virtual:svg-icons-register'
import 'uno.css'

import 'element-plus/theme-chalk/src/message.scss'
import { setupPinia } from './store'
import { setupDirectives } from './directive'
import { setupRouter } from './router'
import { initApp, setupLoading } from './hooks'

async function setupApp() {
	//首次加载或刷新加载动画页
	setupLoading()

	const app = createApp(App)
	// 配置 pinia
	setupPinia(app)

	// 注册全局指令
	setupDirectives(app)

	// 初始化应用
	initApp()

	// // 配置路由
	await setupRouter(app)

	app.mount('#app')
}

setupApp()
