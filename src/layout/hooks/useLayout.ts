import { useMenuStoreWithout, useSettingStore } from '@/store'

export function useLayout() {
	const { splitMenu } = storeToRefs(useMenuStoreWithout())
	const { appSetting } = useSettingStore()
	const { app, layout, tags, footer, menu, header } = appSetting

	const styles = reactive<LayoutType.LayoutStyle>({
		layoutHeader: 'layout-header',
		layoutHeaderPlacement: 'layout-header_placement',
		layoutTags: 'layout-tags',
		layoutTagsPlacement: 'layout-tags_placement',
		layoutAside: 'layout-aside',
		layoutAsideCollapsed: 'layout-aside_collapsed',
		layoutFooter: 'layout-footer',
		layoutFooterPlacement: 'layout-footer_placement',
		leftGap: 'left-gap',
		leftGapCollapsed: 'left-gap_collapsed',
		asidePaddingTop: 'aside-padding-top',
		asidePaddingBottom: 'aside-padding-bottom'
	})

	const appTitle = computed(() => app.title)
	// 验证layout
	const validateLayout = (name: LayoutType.LayoutMode) => {
		return layout.mode === name
	}
	const commonClass = ref<string>('transition-all-300')
	const fixedHeaderAndTags = computed(() => layout.fixedHeaderAndTags || validateLayout('classic'))

	const showHeader = computed(() => true)
	const showTags = computed(() => tags.visible)
	const showAside = computed(() => {
		if (validateLayout('transverse')) return false
		if (validateLayout('classic') && menu.splitMenu && splitMenu.value.list.length == 0) return false
		return true
	})
	const showFooter = computed(() => footer.visible)

	const leftGapClass = computed(() => {
		if (showAside.value) {
			return collapse.value ? styles.leftGapCollapsed : styles.leftGap
		}

		return ''
	})

	const headerLeftGapClass = computed(() => (!validateLayout('classic') ? leftGapClass.value : ''))
	const footerLeftGapClass = computed(() => {
		if (!footer.fixed) {
			return leftGapClass.value
		}

		return ''
	})
	// 菜单折叠状态
	const collapse = computed(() => {
		//分类模式下切割菜单没有数据时，折叠起来
		if (validateLayout('columns') && splitMenu.value.list.length == 0) return true
		return menu.collapse
	})
	// 目录手风琴
	const isMenuUniqueOpened = computed(() => {
		return menu.uniqueOpened
	})
	// 改变菜单折叠状态
	const changeCollapse = () => {
		if ((validateLayout('classic') && menu.splitMenu) || validateLayout('transverse')) return
		menu.collapse = !menu.collapse
	}
	// asideLogo状态
	const asideLogo = computed(() => {
		if (validateLayout('classic') || validateLayout('columns')) return false
		return layout.logo
	})
	// headerLogo状态
	const headerLogo = computed(() => {
		if (validateLayout('default') || validateLayout('columns')) return false
		return layout.logo
	})
	const aSidePaddingClass = computed(() => {
		let cls = ''

		if (showHeader.value && !headerLeftGapClass.value) {
			cls += styles.asidePaddingTop
		}
		if (showFooter.value && !footerLeftGapClass.value) {
			cls += ` ${styles.asidePaddingBottom}`
		}

		return cls
	})
	const fixedFooter = computed(() => footer.fixed)
	const breadcrumb = computed(() => header.crumb)
	// 设置全局组件尺寸
	const changeComponentSize = (size: LayoutType.SizeMode) => {
		layout.componentSize = size
	}
	// 打开全局配置
	const openSettingDrawer = () => {
		layout.settingDrawer = true
	}
	// 布局切换
	const onSetLayout = (mode: LayoutType.LayoutMode) => {
		layout.mode = mode
		menu.collapse = false
	}
	// 显示折叠按钮
	const showCollapseBtn = computed(() => {
		if (menu.showCollapseBtn) {
			if (validateLayout('columns') && splitMenu.value.list.length == 0) {
				return false
			}
			if (validateLayout('classic') && menu.splitMenu && splitMenu.value.list.length == 0) {
				return false
			}
		}
		return menu.showCollapseBtn
	})
	return {
		appTitle,
		styles,
		commonClass,
		fixedHeaderAndTags,
		fixedFooter,
		showHeader,
		showTags,
		showAside,
		showFooter,
		asideLogo,
		headerLogo,
		leftGapClass,
		headerLeftGapClass,
		collapse,
		isMenuUniqueOpened,
		aSidePaddingClass,
		footerLeftGapClass,
		breadcrumb,
		showCollapseBtn,
		validateLayout,
		changeCollapse,
		changeComponentSize,
		openSettingDrawer,
		onSetLayout
	}
}
