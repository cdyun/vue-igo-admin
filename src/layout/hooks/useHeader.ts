import { PageEnum } from '@/constants'
import { usePage } from '@/hooks'
import { useAuthStore, useLockStore, useUserStore } from '@/store'
import { creatCode } from '@/utils'

export function useHeader() {
	const { logout } = useUserStore()
	const { routeList } = useAuthStore()
	const { setLockInfo } = useLockStore()
	const { routerPush } = usePage(false)

	const route = useRoute()

	// 用户头像下拉菜单点击时
	const onHandleCommandClick = async (path: string) => {
		if (path === 'home') {
			routerPush(PageEnum.BASE_HOME)
		} else if (path === 'password') {
			routerPush(PageEnum.BASE_PASSWORD)
		} else if (path === 'lock') {
			await setLockInfo({ pwd: creatCode(5, 1).toLocaleLowerCase(), isLock: true, link: route.fullPath })
			routerPush(PageEnum.BASE_LOCK)
		} else if (path === 'logOut') {
			logout(true)
		} else {
			routerPush(path)
		}
	}
	// 定义通知Notice变量内容
	const noticeList = ref<LayoutType.NoticeModel[]>([])

	// 全部已读点击
	const setNoticeAllRead = () => {
		// 后端交互待完善
		noticeList.value = []
	}

	// 前往通知中心点击
	const goToNoticeCenter = () => {
		routerPush(PageEnum.BASE_HOME)
	}

	// 定义Header Search变量内容
	const headerSearch = reactive<LayoutType.HeaderSearchModel>({
		isShowSearch: false,
		menuQuery: '',
		menuList: []
	})

	const headerSearchMenuRef = ref()
	// 搜索弹窗打开
	const openHeaderSearch = () => {
		headerSearch.menuQuery = ''
		headerSearch.isShowSearch = true
		initSearchMenu()
		nextTick(() => {
			setTimeout(() => {
				headerSearchMenuRef.value
			})
		})
	}
	// 初始化菜单数据
	const initSearchMenu = () => {
		if (headerSearch.menuList.length > 0) return false
		function splitMenu(arr: MenuType.Menu[]) {
			arr.map((v: MenuType.Menu) => {
				if (!v.meta?.isHide) headerSearch.menuList.push({ ...v })
				if (v.children) splitMenu(v.children)
			})
		}
		splitMenu(routeList)
	}
	// 菜单搜索数据过滤
	// eslint-disable-next-line @typescript-eslint/ban-types
	const menuSearch = (queryString: string, cb: Function) => {
		const results = queryString ? headerSearch.menuList.filter(createFilter(queryString)) : headerSearch.menuList
		cb(results)
	}
	// 菜单搜索过滤
	const createFilter = (queryString: string) => {
		return (restaurant: RouterType.RouteItem) => {
			return (
				restaurant.path.toLowerCase().indexOf(queryString.toLowerCase()) > -1 ||
				restaurant.meta!.title!.toLowerCase().indexOf(queryString.toLowerCase()) > -1 ||
				restaurant.meta!.title!.indexOf(queryString.toLowerCase()) > -1
			)
		}
	}
	// 当前菜单选中时
	const onSearchMenuSelect = (item: RouterType.RouteItem) => {
		const { meta, path, redirect } = item
		if (item.meta?.isLink && !item.meta?.isIframe) window.open(item.meta?.isLink)
		else if (redirect) routerPush(redirect as string)
		else routerPush(path)
		headerSearch.menuQuery = meta.title
		headerSearch.isShowSearch = false
	}
	return {
		noticeList,
		headerSearch,
		headerSearchMenuRef,
		openHeaderSearch,
		menuSearch,
		onSearchMenuSelect,
		setNoticeAllRead,
		goToNoticeCenter,
		onHandleCommandClick
	}
}
