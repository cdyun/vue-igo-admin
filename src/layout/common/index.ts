import LayoutAside from './layout-aside/index.vue'
import LayoutFooter from './layout-footer/index.vue'
import LayoutHeader from './layout-header/index.vue'
import LayoutLogo from './layout-logo/index.vue'
import LayoutContent from './layout-content/index.vue'
import LayoutSetting from './layout-setting/index.vue'
import LayoutTags from './layout-tags/index.vue'
export { LayoutAside, LayoutFooter, LayoutHeader, LayoutLogo, LayoutContent, LayoutSetting, LayoutTags }
