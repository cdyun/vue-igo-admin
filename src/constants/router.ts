export const LAYOUT = () => import('@/layout/index.vue')

export const EXCEPTION_NAME = 'PageNotFound'
export const EXCEPTION_COMPONENT = () => import('@/views/sys/exception/404.vue')

export const LOGIN_NAME = 'Login'
export const LOGIN_COMPONENT = () => import('@/views/sys/login/index.vue')

export const LOCK_NAME = 'Lock'
export const LOCK_COMPONENT = () => import('@/views/sys/lock/index.vue')

export const PASSWORD_NAME = 'Password'
export const PASSWORD_COMPONENT = () => import('@/views/sys/password/index.vue')

export const REDIRECT_NAME = 'Redirect'
export const REDIRECT_COMPONENT = () => import('@/views/sys/redirect/index.vue')

export enum PageEnum {
	// basic login path
	BASE_LOGIN = '/login',
	// basic home path
	BASE_HOME = '/dashboard',
	// error page path
	ERROR_PAGE = '/exception',
	// error log page path
	ERROR_LOG_PAGE = '/error-log/list',
	// 锁屏，
	BASE_LOCK = '/lock',
	// 修改密码
	BASE_PASSWORD = '/password'
}
