/**
 * Namespace FormType
 */
declare namespace FormType {
	import type { FormInstance, FormRules } from 'element-plus'
	type FormInstance = FormInstance
	type FormRules = FormRules

	interface LoginFormModel {
		username: string
		password: string
		code: string
	}
}
