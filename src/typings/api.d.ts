/**
 * Namespace ApiType
 */
declare namespace ApiType {
	//登录提交
	interface LoginParams {
		username: string
		password: string
	}

	//登录成功结果
	interface LoginResultModel {
		access_token: string
	}
}
