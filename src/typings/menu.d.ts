/**
 * Namespace MenuType
 */
declare namespace MenuType {
	import { RouteMeta } from 'vue-router'

	// 目录标签
	interface MenuTag {
		type?: 'primary' | 'error' | 'warn' | 'success'
		content?: string
		dot?: boolean
	}

	// 目录
	interface Menu {
		//  菜单名
		name: string
		// 菜单路径
		path: string
		// 路径包含参数，自动分配。
		paramPath?: string
		// 菜单图标,如果没有，则会尝试使用route.meta.icon
		icon?: string
		// 菜单图片,如果同时传递了icon和img,则只会显示img
		img?: string
		// 是否禁用
		disabled?: boolean
		// 子菜单
		children?: Menu[]
		// 菜单标签设置
		tag?: MenuTag
		// 是否隐藏菜单
		hideMenu?: boolean
		// 排序
		orderNo?: number
		// 角色标识
		roles?: string[]
		meta?: Partial<RouteMeta>
		label?: Node | JSX.Element | string
		key?: string | number
	}
}
