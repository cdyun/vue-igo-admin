/**
 * Namespace StoreType
 */
declare namespace StoreType {
	interface LayoutSettingModel {
		// 打开配置
		settingDrawer: boolean
		// 布局模式
		mode: LayoutMode
		// 主题色
		primary: string
		// 固定头部和标签栏
		fixedHeaderAndTags: boolean
		logo: boolean
		componentSize: SizeMode
	}
	// 面包屑
	interface CrumbSettingModel {
		visible: boolean
		showIcon: boolean
	}
	// 头部
	interface HeaderSettingModel {
		height: number
		bgColor: string
		textColor: string
		crumb: CrumbSettingModel
	}
	// 标签
	interface TagsSettingModel {
		visible: boolean
		showIcon: boolean
		height: number
		mode: LayoutType.TagsStyleMode
		isCache: boolean
	}

	// 侧边栏
	interface SideSettingModel {
		width: number
		collapsedWidth: number
		mixCollapsedWidth: number
		mixChildMenuWidth: number
		bgColor: string
		textColor: string
		mixActiveColor: boolean
		mixChildBgColor: string
		mixChildTextColor: string
	}
	// 菜单
	interface MenuSettingModel {
		showCollapseBtn: boolean
		collapse: boolean
		splitMenu: boolean
		uniqueOpened: boolean
	}
	// 页脚
	interface FooterSettingModel {
		visible: boolean
		fixed: boolean
		align: LayoutType.AlignMode
		height: number
		bgColor: string
		textColor: string
	}
	// 页面
	interface PageSettingModel {
		animate: boolean
		animateMode: LayoutType.AnimationMode
		lockScreen: boolean
		lockScreenTime: number
		isGrayscale: boolean
		isWartermark: boolean
		wartermarkText: string
	}
	// APP应用
	interface AppSettingModel {
		authRoute: EnvType.AuthRouteMode
		routerHistory: EnvType.RouterHistoryMode
		title: string
		desc: string
		shortName: string
	}
	// store布局配置状态
	interface SettingModel {
		[key: string]: any
		app: AppSettingModel
		layout: LayoutSettingModel
		header: HeaderSettingModel
		tags: TagsSettingModel
		side: SideSettingModel
		menu: MenuSettingModel
		footer: FooterSettingModel
		page: PageSettingModel
	}
	// 用户信息
	interface UserInfo {
		id: string | number
		username: string
		nickname: string
		avatar: string
		desc?: string
		homePath?: string
		roles: RoleInfo[]
	}
	// store锁屏信息
	interface LockState {
		pwd?: string | undefined
		isLock?: boolean
		link?: string
	}
}
