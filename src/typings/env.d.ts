/**
 * Namespace EnvType
 */
declare namespace EnvType {
	//  路由模式类型
	type RouterHistoryMode = 'hash' | 'history' | 'memory'
	// 路由来源
	type AuthRouteMode = 'static' | 'dynamic'

	interface ImportMeta extends ImportMetaEnv {
		//  应用基础URL
		readonly VITE_APP_URL: string
		//  应用标题
		readonly VITE_APP_TITLE: string
		//  应用名称缩写，字母数字组成，不要有其他符号
		readonly VITE_APP_SHORT_NAME: string
		//  应用描述
		readonly VITE_APP_DESC: string
		//  本地环境
		readonly ENV: 'development' | 'production'
		//  port 端口号
		readonly VITE_PORT: number
		//  代理配置，请注意，没有换行符
		readonly VITE_PROXY: string
		//  接口服务url前缀
		readonly VITE_BASE_URL: string
		//  路由 history mode
		readonly VITE_ROUTER_HISTORY_MODE?: RouterHistoryMode

		//   路由来源 - Static: 前端 - Dynamic: 后端
		readonly VITE_AUTH_ROUTE_MODE: AuthRouteMode

		//  open 运行 npm run dev 时自动打开浏览器
		readonly VITE_OPEN: string
	}
}
