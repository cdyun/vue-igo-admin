type Recordable<T = any> = Record<string, T>
declare type IntervalHandle = ReturnType<typeof setInterval>
