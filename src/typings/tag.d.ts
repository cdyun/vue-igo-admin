/**
 * Namespace TagType
 */
declare namespace TagType {
	type TagMenuKey =
		| 'fullscreenTag'
		| 'refreshTag'
		| 'closeCurrent'
		| 'closeOther'
		| 'closeLeft'
		| 'closeRight'
		| 'closeAll'
	type TagMenuOption = {
		key: TagType.TagMenuKey
		label: string
		icon?: () => VNode
		disabled?: boolean
	}
	// 标签路由
	type TagRoute = Pick<RouteLocationNormalizedLoaded, 'name' | 'path' | 'meta'> &
		Partial<Pick<RouteLocationNormalizedLoaded, 'fullPath' | 'query'>>

	// tags类型
	interface TagState {
		id: string
		label: string
		routeKey: string
		routePath: string
		fullPath: string
		icon?: string
	}
}
