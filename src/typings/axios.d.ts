/**
 * Namespace AxiosType
 */
declare namespace AxiosType {
	// 错误消息提示类型
	type ErrorMessageMode = 'none' | 'modal' | 'message' | undefined

	// 请求Options
	interface RequestOptions {
		// 将请求参数拼接到url
		joinParamsToUrl?: boolean
		// 格式化请求参数时间
		formatDate?: boolean
		// 是否处理请求结果
		isTransformResponse?: boolean
		// 是否返回本机响应标头，当您需要获取响应标头时，请使用此属性
		isReturnNativeResponse?: boolean
		// 接口地址，如果保留为空，请使用默认的apiUrl
		apiUrl?: string | (() => string)
		// 错误消息提示类型
		errorMessageMode?: ErrorMessageMode
		// 是否添加时间戳
		joinTime?: boolean
		ignoreCancelToken?: boolean
		// 是否在标头中发送令牌
		withToken?: boolean
	}

	// 请求结果
	interface RequestResult<T = any> {
		code: number
		type: 'success' | 'error' | 'warning'
		message: string
		result: T
	}
}
