/**
 * Namespace LayoutType
 */
declare namespace LayoutType {
	type SizeMode = '' | 'default' | 'small' | 'large'
	type LayoutMode = 'default' | 'classic' | 'transverse' | 'columns'
	type TagsStyleMode = 'tags-style-one' | 'tags-style-two' | 'tags-style-three'
	type AnimationMode = 'fade' | 'fade-slide' | 'fade-bottom' | 'fade-scale' | 'zoom-fade' | 'zoom-out'
	type AlignMode = 'left' | 'center' | 'right'

	// navBars search
	interface HeaderSearchModel {
		isShowSearch: boolean
		menuQuery: string
		menuList: Menu[]
	}
	// Header Notice
	interface NoticeModel {
		label: string
		value: string
		time: string
	}

	// 布局 style
	interface LayoutStyle {
		readonly layoutHeader: string
		readonly layoutHeaderPlacement: string
		readonly layoutTags: string
		readonly layoutTagsPlacement: string
		readonly layoutAside: string
		readonly layoutAsideCollapsed: string
		readonly layoutFooter: string
		readonly layoutFooterPlacement: string
		readonly leftGap: string
		readonly leftGapCollapsed: string
		readonly asidePaddingTop: string
		readonly asidePaddingBottom: string
	}

	interface LayoutMenuMode {
		list: Menu[]
		visible: boolean
		defaultActive: string | unknown
	}

	interface LayoutMenuMode {
		list: Menu[]
		visible: boolean
		defaultActive: string
	}
}
