import { PageEnum } from '@/constants'
import { router as globalRouter } from '@/router'
import { RouteLocationRaw } from 'vue-router'

export type PathAsPageEnum<T> = T extends { path: string } ? T & { path: PageEnum } : T
export type RoutePathEx = PathAsPageEnum<RouteLocationRaw>
/**
 * @param inSetup 判断是否在script setup使用
 */
export function usePage(inSetup = true) {
	function handleError(e: Error) {
		console.error(e)
	}
	const router = inSetup ? useRouter() : globalRouter
	const routerGo = async (opt: RoutePathEx = PageEnum.BASE_HOME, isReplace = false) => {
		if (!opt) {
			return
		}
		isReplace ? router.replace(opt).catch(handleError) : router.push(opt).catch(handleError)
	}
	const routerPush = router.push
	const routerBack = router.back
	return { routerGo, routerPush, routerBack }
}
