export function setupLoading() {
	const loading = `
<div class="h-full w-full flex flex-justify-center flex-items-center flex-col bg-#ffffff"  >
		<div class="line-scale-pulse-out" style='color: red'>
				<div></div>
				<div></div>
				<div></div>
				<div></div>
				<div></div>
		</div>
<!--  <div style='font-size: 22px;padding-top: 30px'>加载中...</div>-->
</div>`;

	const app = document.getElementById('app');

	if (app) {
		app.innerHTML = loading;
	}
}
