import { useSettingStoreWithout } from '@/store'
import { batchCssVar, setCssVar, getEnv } from '@/utils'

//初始化应用
export function initApp() {
	const { appSetting } = storeToRefs(useSettingStoreWithout())

	appSetting.value.app = {
		authRoute: getEnv('VITE_AUTH_ROUTE_MODE'),
		routerHistory: getEnv('VITE_ROUTER_HISTORY_MODE'),
		title: getEnv('VITE_APP_TITLE'),
		desc: getEnv('VITE_APP_DESC'),
		shortName: getEnv('VITE_APP_SHORT_NAME')
	}
	// useSettingStoreWithout().setAppSetting({
	// 	app: {
	// 		authRoute: getEnv('VITE_AUTH_ROUTE_MODE'),
	// 		routerHistory: getEnv('VITE_ROUTER_HISTORY_MODE'),
	// 		title: getEnv('VITE_APP_TITLE'),
	// 		desc: getEnv('VITE_APP_DESC'),
	// 		shortName: getEnv('VITE_APP_SHORT_NAME')
	// 	}
	// })
	const { layout, header, tags, side, footer } = appSetting.value
	watchEffect(() => {
		batchCssVar('--el-color-primary', layout.primary)
		setCssVar('--self-header-bg', header.bgColor)
		setCssVar('--self-header-color', header.textColor)
		setCssVar('--self-header-height', header.height + 'px')
		setCssVar('--self-tags-height', tags.height + 'px')
		setCssVar('--self-aside-bg', side.bgColor)
		setCssVar('--self-aside-color', side.textColor)
		setCssVar('--self-aside-mix-bg', side.mixChildBgColor)
		setCssVar('--self-aside-mix-color', side.mixChildTextColor)
		setCssVar('--self-footer-bg', footer.bgColor)
		setCssVar('--self-footer-color', footer.textColor)
		setCssVar('--self-footer-height', footer.height + 'px')
		if (layout.mode === 'columns') {
			setCssVar('--self-aside-width', side.mixCollapsedWidth + side.mixChildMenuWidth + 'px')
			setCssVar('--self-aside-collapsed-width', side.mixCollapsedWidth + 'px')
		} else {
			setCssVar('--self-aside-width', side.width + 'px')
			setCssVar('--self-aside-collapsed-width', side.collapsedWidth + 'px')
		}
		if (layout.mode === 'classic' || layout.mode === 'transverse') {
			setCssVar('--self-aside-z-index', '96')
		} else {
			setCssVar('--self-aside-z-index', '99')
		}
	})
}
