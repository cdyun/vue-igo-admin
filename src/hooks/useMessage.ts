import { ElMessage, ElMessageBox, ElMessageBoxOptions, MessageParamsWithType, ElNotification, NotificationParams, MessageParams } from 'element-plus'

const getBaseOptions = () => {
	return {
		confirmButtonText: '知道了',
		showClose: false,
		showCancelButton: false,
		closeOnClickModal: false,
	}
}

function createModalOptions(options: ElMessageBoxOptions): ElMessageBoxOptions {
	return {
		...getBaseOptions(),
		...options,
	}
}

export function modalSuccess(options: ElMessageBoxOptions) {
	options.type = 'success'
	options.title = '提示'
	return ElMessageBox(createModalOptions(options))
}

export function modalError(options: ElMessageBoxOptions) {
	options.type = 'error'
	options.title = '错误提示'
	return ElMessageBox(createModalOptions(options))
}

export function messageSuccess(options?: MessageParamsWithType) {
	return ElMessage.success(options)
}

export function messageError(options?: MessageParamsWithType) {
	return ElMessage.error(options)
}
export function message(options?: MessageParams) {
	return ElMessage(options)
}

export function notification(options?: NotificationParams) {
	return ElNotification(options)
}

export function useMessage() {
	return {
		modalSuccess,
		modalError,
		messageSuccess,
		messageError,
		notification,
		message,
	}
}
