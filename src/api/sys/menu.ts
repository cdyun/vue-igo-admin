import { request } from '@/network'

enum ApiUrl {
	GetMenuList = '/system/get_menu'
}
/**
 * @description: 获取登录用户的菜单
 */
export const getMenuListApi = (mode: AxiosType.ErrorMessageMode = 'message') => {
	return request.get<MenuType.Menu[]>({ url: ApiUrl.GetMenuList }, { errorMessageMode: mode })
}
