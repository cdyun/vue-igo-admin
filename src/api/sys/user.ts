import { request } from '@/network'

enum ApiUrl {
	Login = '/login/do_login',
	Logout = '/logout',
	Password = '/system/password',
	GetUserInfo = '/system/get_user',
	GetPermCode = '/system/get_perm'
}

export function doLoginApi(params: ApiType.LoginParams, mode: AxiosType.ErrorMessageMode = 'message') {
	return request.post<ApiType.LoginResultModel>({ url: ApiUrl.Login, params }, { errorMessageMode: mode })
}
export function getUserInfoApi(mode: AxiosType.ErrorMessageMode = 'message') {
	return request.get<StoreType.UserInfo>({ url: ApiUrl.GetUserInfo }, { errorMessageMode: mode })
}

export function getPermCodeApi() {
	return request.get<string[]>({ url: ApiUrl.GetPermCode })
}
export function doLogoutApi() {
	return request.get({ url: ApiUrl.Logout })
}
