import { request } from '@/network'

enum ApiUrl {
	GetRouteList = '/system/get_menu'
}
/**
 * @description: 获取登录用户的权限路由
 */
export const getRouteListApi = (mode: AxiosType.ErrorMessageMode = 'message') => {
	return request.get<RouterType.RouteItem[]>({ url: ApiUrl.GetRouteList }, { errorMessageMode: mode })
}
