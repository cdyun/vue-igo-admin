import { request } from '@/network'
enum ApiUrl {
	GetAppConfig = '/system/config'
}
export function getAppConfigApi() {
	return request.get({ url: ApiUrl.GetAppConfig })
}
