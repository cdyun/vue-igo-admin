// 获取env配置指定key
export function getEnv(key: string) {
	const env = import.meta.env as EnvType.ImportMeta
	if (env[key] === undefined) {
		throw new Error('请检查.env配置文件是否设置了' + key)
	}
	return env[key]
}

// 获取env MODE
export function getEnvMode() {
	return import.meta.env.MODE
}
