import { version } from '../../package.json'
import { getEnv, getEnvMode } from './env'

//存储key前缀
function createStorageKeyPrefix() {
	return `${getEnv('VITE_APP_SHORT_NAME')}_${getEnvMode()}`
}

// 存储key全称
export function createStorageName() {
	return `${createStorageKeyPrefix()}${`_${version}`}`
}
