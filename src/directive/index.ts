import type { App } from 'vue'
import { wavesDirective } from '@/directive/wavesDirective'
import { reClickDirective } from '@/directive/reClickDirective'

/**
 * 导出指令方法：v-xxx
 * @methods wavesDirective 按钮波浪指令，用法：v-waves
 * @methods reClickDirective 防重复点击，用法：v-reClick
 */

export function setupDirectives(app: App) {
	// 按钮波浪指令
	wavesDirective(app)
	//防重复点击(指令实现)
	reClickDirective(app)
}
