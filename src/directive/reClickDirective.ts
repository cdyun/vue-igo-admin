import type { App } from 'vue'

/**
 * @directive 防重复点击(指令实现)
 */
export function reClickDirective(app: App) {
	app.directive('reClick', {
		mounted: function (el) {
			el.addEventListener('click', () => {
				if (!el.disabled) {
					el.disabled = true
					setTimeout(() => {
						el.disabled = false
					}, 2500)
				}
			})
		}
	})
}
