import {
	EXCEPTION_COMPONENT,
	EXCEPTION_NAME,
	LAYOUT,
	LOCK_COMPONENT,
	LOCK_NAME,
	LOGIN_COMPONENT,
	LOGIN_NAME,
	PASSWORD_COMPONENT,
	PASSWORD_NAME,
	PageEnum,
	REDIRECT_COMPONENT,
	REDIRECT_NAME
} from '@/constants'
/**
 * 定义根路由
 */
export const ROOT_ROUTE: RouterType.RouteItem = {
	path: '/',
	name: 'Root',
	redirect: PageEnum.BASE_HOME,
	meta: {
		title: 'Root'
	}
}
/**
 * 定义前端添加路由
 */
export const frontRoutes: Array<RouterType.RouteItem> = [
	{
		path: '/dashboard',
		component: LAYOUT,
		redirect: '/dashboard/workbench',
		name: 'Dashboard',
		meta: {
			title: '首页',
			icon: 'mdi:monitor-dashboard'
		},
		children: [
			{
				path: 'workbench',
				component: () => import('@/views/sys/dashboard/workbench.vue'),
				name: 'Workbench',
				meta: {
					title: 'workbench',
					hideMenu: true,
					affix: true
				}
			}
		]
	},
	{
		path: '/system',
		component: LAYOUT,
		redirect: '/system/user',
		name: 'System',
		meta: {
			title: '系统管理',
			icon: 'icon-park-twotone:setting-computer'
		},
		children: [
			{
				path: 'user',
				component: () => import('@/views/system/user/index.vue'),
				name: 'SystemUser',
				meta: {
					title: '用户管理',
					keepAlive: true
				}
			},
			{
				path: 'role',
				component: () => import('@/views/system/role/index.vue'),
				name: 'SystemRole',
				meta: {
					title: '角色管理',
					keepAlive: true
				}
			},
			{
				path: 'node',
				component: () => import('@/views/system/node/index.vue'),
				name: 'SystemNode',
				meta: {
					title: '路由管理',
					keepAlive: true
				}
			}
		]
	},
	{
		path: '/about',
		component: LAYOUT,
		name: 'About',
		redirect: '/about/index',
		meta: {
			title: '关于',
			icon: 'fluent:book-information-24-regular'
		},
		children: [
			{
				path: 'index',
				component: () => import('@/views/about/index.vue'),
				name: 'About',
				meta: {
					title: '关于',
					hideMenu: true,
					keepAlive: true
				}
			}
		]
	}
]

// 定义404 on a page
export const PAGE_NOT_FOUND_ROUTE: RouterType.RouteItem = {
	path: '/:path(.*)*',
	name: EXCEPTION_NAME,
	component: LAYOUT,
	meta: {
		title: '访问错误',
		hideBreadcrumb: true,
		hideMenu: true
	},
	children: [
		{
			path: '/:path(.*)*',
			name: EXCEPTION_NAME,
			component: EXCEPTION_COMPONENT,
			meta: {
				title: '访问错误',
				hideBreadcrumb: true,
				hideMenu: true
			}
		}
	]
}

/**
 * 定义静态路由（默认路由）
 * 前端添加路由，请在顶级节点的 `children 数组` 里添加
 */
export const staticRoutes: Array<RouterType.RouteItem> = [
	//根路由
	ROOT_ROUTE,
	// 登录
	{
		path: PageEnum.BASE_LOGIN,
		name: LOGIN_NAME,
		component: LOGIN_COMPONENT,
		meta: {
			title: '登录'
		}
	},
	//修改密码
	{
		path: PageEnum.BASE_PASSWORD,
		name: PASSWORD_NAME,
		component: LAYOUT,
		redirect: PageEnum.BASE_PASSWORD + '/index',
		meta: {
			hideMenu: true,
			title: '修改密码'
		},
		children: [
			{
				path: 'index',
				component: PASSWORD_COMPONENT,
				name: 'HomePassword',
				meta: {
					title: '修改密码',
					hideMenu: true,
					keepAlive: true
				}
			}
		]
	},
	//锁屏
	{
		path: PageEnum.BASE_LOCK,
		name: LOCK_NAME,
		component: LOCK_COMPONENT,
		meta: {
			title: '锁屏'
		}
	},
	//未知路由404
	PAGE_NOT_FOUND_ROUTE,
	//重定向
	{
		path: '/redirect',
		component: LAYOUT,
		name: REDIRECT_NAME,
		meta: {
			title: '重定向',
			hideBreadcrumb: true,
			hideMenu: true
		},
		children: [
			{
				path: '/redirect/:path(.*)/:_redirect_type(.*)/:_origin_params(.*)?',
				name: REDIRECT_NAME,
				component: REDIRECT_COMPONENT,
				meta: {
					title: '重定向',
					hideBreadcrumb: true
				}
			}
		]
	}
]
