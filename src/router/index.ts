import { createRouter, createWebHashHistory } from 'vue-router'
import { App } from 'vue'
import { staticRoutes } from './routes'
import { initAuthGuard, initBasicGuard } from './guard'

export const router = createRouter({
	history: createWebHashHistory(import.meta.env.VITE_BASE_URL),
	routes: [...staticRoutes] as unknown as RouterType.RouteItem[],
	// 是否应该禁止尾部斜杠。默认为假
	strict: true,
	scrollBehavior: () => ({ left: 0, top: 0 })
})

// 配置路由器
export async function setupRouter(app: App<Element>) {
	app.use(router)
	// 初始化基础路由守卫
	initBasicGuard()
	// 初始化权限路由守卫
	initAuthGuard()
	await router.isReady()
}
