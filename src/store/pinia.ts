import type { App } from 'vue'
import { createPinia } from 'pinia'
import { createPersistedState } from 'pinia-plugin-persistedstate'
import { createStorageName } from '@/utils'
import { persistGlobalConfig } from './persist'
import { useAuthStore, useSettingStore, useUserStore, useLockStore, useTagStore, useMenuStore } from '@/store'

const pinia = createPinia()
pinia.use(createPersistedState(persistGlobalConfig(createStorageName())))

export function setupPinia(app: App<Element>) {
	app.use(pinia)
	registerStore()
}

export { pinia }

/**
 * 注册app状态库
 */
export const registerStore = () => {
	useUserStore()
	useAuthStore()
	useSettingStore()
	useLockStore()
	useTagStore()
	useMenuStore()
}
