import { useBoolean } from '@/hooks'
import { pinia } from '@/store'

export const useSettingStore = defineStore(
	'setting',
	() => {
		// 初始配置
		const defaultSetting: StoreType.SettingModel = {
			app: {
				authRoute: 'dynamic',
				routerHistory: 'history',
				title: '管理系统',
				desc: '',
				shortName: 'portal system'
			},
			layout: {
				settingDrawer: false,
				mode: 'default',
				primary: '#646CFF',
				fixedHeaderAndTags: false,
				logo: true,
				componentSize: 'large'
			},
			header: {
				height: 50,
				bgColor: '#ffffff',
				textColor: '#333639',
				crumb: {
					visible: true,
					showIcon: true
				}
			},
			tags: {
				visible: true,
				showIcon: true,
				height: 44,
				mode: 'tags-style-one',
				isCache: true
			},
			side: {
				width: 220,
				collapsedWidth: 64,
				mixCollapsedWidth: 80,
				mixChildMenuWidth: 200,
				bgColor: '#ffffff',
				textColor: '#333639',
				mixActiveColor: true,
				mixChildBgColor: '#ffffff',
				mixChildTextColor: '#333639'
			},
			menu: {
				showCollapseBtn: true,
				collapse: false,
				splitMenu: false,
				uniqueOpened: true
			},
			footer: {
				visible: true,
				fixed: false,
				align: 'center',
				height: 48,
				bgColor: '#ffffff',
				textColor: '#333639'
			},
			page: {
				animate: true,
				animateMode: 'fade-slide',
				lockScreen: false,
				lockScreenTime: 30,
				isGrayscale: false,
				isWartermark: false,
				wartermarkText: 'thzapp'
			}
		}
		// 应用配置
		const appSetting = reactive<StoreType.SettingModel>(JSON.parse(JSON.stringify(defaultSetting)))

		//赋值
		const setAppSetting = (data: StoreType.SettingModel) => {
			Object.entries(data).forEach(([key, value]) => {
				appSetting[key] = value
			})
		}

		//重置
		const resetSetting = () => {
			setAppSetting(JSON.parse(JSON.stringify(defaultSetting)))
		}
		// 刷新页面
		const reloadPage = async (duration = 0) => {
			setReloadFlag(false)
			if (duration > 0) {
				await new Promise((resolve) => {
					setTimeout(resolve, duration)
				})
			}
			setReloadFlag(true)
		}
		const { bool: reloadFlag, setBool: setReloadFlag } = useBoolean(true)
		const { bool: contentXScrollable, setBool: setContentXScrollable } = useBoolean()
		return {
			appSetting,
			setAppSetting,
			resetSetting,
			reloadFlag,
			setReloadFlag,
			contentXScrollable,
			setContentXScrollable,
			reloadPage
		}
	},
	{
		persist: true
	}
)

// 在组合式setup之外使用
export function useSettingStoreWithout() {
	return useSettingStore(pinia)
}
