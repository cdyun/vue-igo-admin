import { cloneDeep } from '@/utils'
import { pinia } from '@/store'
import { useAuthStore } from './auth'
import { usePage } from '@/hooks'
export const useTagStore = defineStore(
	'tags',
	() => {
		// 常驻的Tags
		const affixTags = ref<TagType.TagState[]>([])
		// 动态打开的Tags
		const listTags = ref<TagType.TagState[]>([])
		// KeepAlive Tags
		const cacheTags = ref<string[]>([])
		// Active Tag Id
		const activeId = ref<string>('')
		// 赋值Active ID
		const setActiveId = (id: string) => {
			activeId.value = id
		}
		// 重置Tags
		const resetTags = (): void => {
			affixTags.value = []
			listTags.value = []
			cacheTags.value = []
			activeId.value = ''
		}
		// 初始化Tags
		const buildTagsAction = () => {
			const { routeList } = useAuthStore()
			const listData = cloneDeep(routeList)
			filterRouterFun(listData)
		}
		// 路由过滤递归函数,处理Tag隐藏属性：hideTag,常驻的Tags,KeepAlive Tags
		const filterRouterFun = (list: RouterType.RouteItem[]) => {
			return list
				.filter((item: RouterType.RouteItem) => !item.meta?.hideTag)
				.map((item: RouterType.RouteItem) => {
					const pathName = (item.redirect ? item.redirect : item.path) as string
					if (item.meta.affix && !isInTags(pathName, affixTags.value)) {
						affixTags.value.push({
							id: pathName,
							label: item.meta.title,
							routeKey: item.name,
							routePath: pathName,
							fullPath: pathName,
							icon: item.meta.icon
						})
					}
					if (!item.meta.ignoreKeepAlive && cacheTags.value.indexOf(item.name) == -1) {
						cacheTags.value.push(item.name)
					}
					if (item.children) filterRouterFun(item.children)
				})
		}
		// 添加路由到listTags
		const addTag = (route: TagType.TagRoute, active = true) => {
			const tag = getTagByRoute(route)
			if (!isInTags(tag.id, affixTags.value) && !isInTags(tag.id, listTags.value)) {
				listTags.value.push(tag)
			}
			if (active) {
				setActiveId(tag.id)
			}
		}
		// 通过Route获取TagId
		const getTagIdByRoute = (route: TagType.TagRoute) => {
			const { path, query = {}, meta } = route
			let id = path
			if (meta.carryParam) {
				const queryKeys = Object.keys(query).sort()
				const qs = queryKeys.map((key) => `${key}=${query[key]}`).join('&')
				id = `${path}?${qs}`
			}
			return id
		}
		// 通过Route获取Tag
		const getTagByRoute = (route: TagType.TagRoute) => {
			const { name, path, fullPath = path, meta } = route
			const { title, icon } = meta
			const tag: TagType.TagState = {
				id: getTagIdByRoute(route),
				label: title,
				routeKey: name,
				routePath: path,
				fullPath,
				icon
			}
			return tag
		}
		// 获取指定类型标签的所有Ids
		const getTagsIds = (tags: TagType.TagState[]) => {
			return tags.map((tag) => tag.id)
		}
		// 通过Id过滤Tags
		const filterTagsById = (id: string, tags: TagType.TagState[]) => {
			return tags.filter((tag) => tag.id !== id)
		}
		// 通过Ids过滤Tags
		const filterTagsByIds = (ids: string[], tags: TagType.TagState[]) => {
			return tags.filter((tag) => !ids.includes(tag.id))
		}
		// 判断标签栏是否已打开
		const isInTags = (id: string, tags: TagType.TagState[]) => {
			return tags.some((item) => item.id === id)
		}
		// 判断标签ID是否属于固定标签
		const isAffixTags = (id: string) => {
			return isInTags(id, affixTags.value)
		}
		const { routerPush } = usePage(false)
		// 根据Tag切换路由
		const switchRouteByTag = async (tag: TagType.TagState) => {
			const fail = await routerPush(tag.fullPath)
			if (!fail) {
				setActiveId(tag.id)
			}
		}
		// 关闭当前标签
		const closeTag = async (tagId: string) => {
			const isRemoveActiveTag = activeId.value === tagId
			const updatedTags = filterTagsById(tagId, listTags.value)
			function update() {
				listTags.value = updatedTags
			}
			if (!isRemoveActiveTag) {
				update()
				return
			}
			const activeTag = updatedTags[updatedTags.length - 1] || affixTags.value[0]
			await switchRouteByTag(activeTag)
			update()
		}
		// 关闭其他标签或关闭所有标签
		const closeAllTags = async (excludes: string[] = []) => {
			const affixTagsIds = [...getTagsIds(affixTags.value), ...excludes]
			const removedTagsIds = listTags.value.map((tag) => tag.id).filter((id) => !affixTagsIds.includes(id))
			const isRemoveActiveTag = removedTagsIds.includes(activeId.value)
			const updatedTags = filterTagsByIds(removedTagsIds, listTags.value)
			function update() {
				listTags.value = updatedTags
			}
			if (!isRemoveActiveTag) {
				update()
				return
			}
			const activeTag = updatedTags[updatedTags.length - 1] || affixTags.value[0]
			await switchRouteByTag(activeTag)
			update()
		}
		// 关闭左侧所有标签
		const closeLeftTags = async (tagId: string) => {
			const tagIds = listTags.value.map((tag) => tag.id)
			const index = tagIds.indexOf(tagId)
			if (index === -1) return
			const excludes = tagIds.slice(index)
			await closeAllTags(excludes)
		}
		// 关闭右侧所有标签
		const closeRightTags = async (tagId: string) => {
			const tagIds = listTags.value.map((tag) => tag.id)
			const index = tagIds.indexOf(tagId)
			if (index === -1) return
			const excludes = tagIds.slice(0, index + 1)
			await closeAllTags(excludes)
		}
		return {
			activeId,
			affixTags,
			listTags,
			cacheTags,
			isAffixTags,
			setActiveId,
			buildTagsAction,
			addTag,
			closeTag,
			closeAllTags,
			closeLeftTags,
			closeRightTags,
			switchRouteByTag,
			resetTags
		}
	},
	{
		persist: {
			paths: ['listTags', 'cacheTags', 'activeId']
		}
	}
)
// 在组合式setup之外使用
export function useTagStoreWithout() {
	return useTagStore(pinia)
}
