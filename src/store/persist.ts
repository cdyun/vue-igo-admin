import { PersistedStateFactoryOptions } from 'pinia-plugin-persistedstate'

// pinia持久化全局配置
export function persistGlobalConfig(keyPrefix: string): PersistedStateFactoryOptions {
	return {
		storage: sessionStorage,
		key: (id) => `${keyPrefix}_${id}`.toLocaleLowerCase()
	}
}
