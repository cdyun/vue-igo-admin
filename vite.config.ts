import { defineConfig, loadEnv, ConfigEnv } from 'vite'
import { setupViteBuild, setupVitePlugins, setupViteServer } from './configs'

import { fileURLToPath } from 'node:url'

// https://vitejs.dev/config/
export default defineConfig((configEnv: ConfigEnv) => {
	const env = loadEnv(configEnv.mode, process.cwd()) as unknown as EnvType.ImportMeta
	return {
		plugins: setupVitePlugins(env),
		root: process.cwd(),
		base: env.VITE_APP_URL,
		resolve: {
			alias: {
				'@': fileURLToPath(new URL('./src', import.meta.url))
			}
		},
		css: {
			preprocessorOptions: {
				scss: {
					additionalData: `@use "@/assets/css/element/index.scss" as *;`
				}
			}
		},
		server: setupViteServer(env),
		build: setupViteBuild(),
		define: {
			__APP_VERSION__: JSON.stringify(process.env.npm_package_version),
			__APP_NAME__: JSON.stringify('vue-admin')
		}
	}
})
