import path from 'path'

export function setupViteBuild() {
	return {
		outDir: path.resolve(__dirname, `../../dist`), // 指定输出路径
		assetsInlineLimit: 4096, //小于此阈值的导入或引用资源将内联为 base64 编码，以避免额外的 http 请求
		emptyOutDir: true, //Vite 会在构建时清空该目录
		rollupOptions: {
			output: {
				assetFileNames: 'assets/[ext]/[name]-[hash].[ext]',
				chunkFileNames: 'assets/js/[name]-[hash].js',
				entryFileNames: 'assets/js/[name]-[hash].js',
				compact: true,
				manualChunks: (id: string) => {
					if (id.includes('node_modules')) {
						const name = id.toString().split('node_modules/')[1].split('/')[0].toString()
						return name == '.pnpm' ? 'vender' : name // 拆分多个vendors
					}
				}
			}
		}
	}
}
